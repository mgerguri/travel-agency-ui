import axios from 'axios'

export default {
    async getAllPosts() {
        const res = await axios.get('http://localhost:8090/posts')

        return res.data
    },
    async postComment(payload) {
        const res = await axios.post('http://localhost:8090/comments', payload)

        return res.data
    },
    async getComments() {
        const res = await axios.get('http://localhost:8090/comments')

        return res.data
    }
}
