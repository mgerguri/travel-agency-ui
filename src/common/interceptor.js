import Axios from 'axios'

Axios.defaults.headers.common.Accept = 'application/json';

Axios.interceptors.response.use((response) => {
    return response;
}, (error) => {
    return Promise.reject(error);
});

Axios.interceptors.request.use((config) => {
    return config;
}, (error) => {
    return Promise.reject(error);
});
