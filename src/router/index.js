import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import ErrorView from "@/views/ErrorView";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/not-found',
    name: 'ErrorView',
    component: ErrorView
  }
]

const router = new VueRouter({
  routes
})

export default router
